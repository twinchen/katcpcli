#!/usr/bin/python3
import asyncio
import requests
import aiokatcp
import argparse


class TestServer(aiokatcp.DeviceServer):
    """
    Test katcp server
    """

    VERSION = "test-server-api-1.0"
    BUILD_STATE = "test-server-1.0.1.dev0"

    def __init__(self, *args, **kwargs):
        aiokatcp.DeviceServer.__init__(self, *args, **kwargs)

        _sensor = aiokatcp.Sensor(float,
            "dummysensor",
            description="A dummy sensor",
            default=123.4)
        self.sensors.add(_sensor)

    async def request_foo(self, ctx):  # pylint: disable=unused-argument
        """Reply foo"""
        print("Foo request received")
        return "Foo reply!"

    async def request_bar(self, ctx):  # pylint: disable=unused-argument
        """Reply bar"""
        print("Bar request received")

    async def request_fail(self, ctx):  # pylint: disable=unused-argument
        """A reply that fails"""
        raise aiokatcp.FailReply("You requested a fail!")

    async def request_long(self, ctx):  # pylint: disable=unused-argument
        """A long reply"""

        msg = " ".join(99 * ["bottle"] + ["of", "beer", "hanging on a wall"])
        return msg


async def main(network, port):
    """
    Starting server.
    """
    server = TestServer(network, port)
    print("Starting test server ...")
    await server.start()
    await server.join()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        "-a", "--host", help="IP(s) to accept connections from.", default="localhost"
    )
    parser.add_argument("-p", "--port", help="Port to listen on.", default=1235)

    args = parser.parse_args()

    asyncio.get_event_loop().run_until_complete(main(args.host, args.port))
    asyncio.get_event_loop().close()
