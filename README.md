KATCPcli
========
KATCPcli - a command-line client to control KATCP devices.

To use without installation, use the `run` script. To install, use `pip install . -r requirements.txt`.


Run as `$ katcpcli` for a full screen terminal application, or `$ katcpcli
--nofs` for a simpler prompt.


