PYTHONPATH="src" python3-coverage run --source=src/katcpcli tests/test.py
python3-coverage html --include="src/katcpcli/*"
python3-coverage report --include="src/katcpcli/*"
